//
//  FlipboardLabel.h
//  FlipboardDemo
//
//  Created by Rob van der Veer on 2/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlipboardLabel : UIView
@property (nonatomic) NSInteger value;  //animatable.
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) UIImageView *currentTop;
@property (nonatomic, strong) UIImageView *currentBottom;
@property (nonatomic, strong) UIImageView *nextTop;
@property (nonatomic, strong) UIImageView *nextBottom;

@end
