//
//  ViewController.h
//  FlipboardDemo
//
//  Created by Rob van der Veer on 2/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
-(IBAction)startCount:(id)sender;
@end
