//
//  ViewController.m
//  FlipboardDemo
//
//  Created by Rob van der Veer on 2/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ViewController.h"
#import "FlipboardLabel.h"

@interface ViewController ()
{
    FlipboardLabel *label;
    FlipboardLabel *label2;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    label = [[FlipboardLabel alloc] initWithFrame:CGRectMake(0,0, 100, 150)];
    [[self view] addSubview:label];
    label2 = [[FlipboardLabel alloc] initWithFrame:CGRectMake(100,0, 80, 120)];
    [[self view] addSubview:label2];
    
    //label.value = 4;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startCount:(id)sender
{
    int number = arc4random() % 100;
    
    int num1 = number / 10;
    int num2 = number - (num1 * 10);
    
    label.value = num1;
    label2.value = num2;
}
@end
