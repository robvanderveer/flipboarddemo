//
//  FlipboardLabel.m
//  FlipboardDemo
//
//  Created by Rob van der Veer on 2/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "FlipboardLabel.h"
#import <QuartzCore/QuartzCore.h>

@interface imageData : NSObject
@property (nonatomic, strong) UIImage *topHalf;
@property (nonatomic, strong) UIImage *bottomHalf;
@end

@implementation imageData
@end

@interface FlipboardLabel()
@property (nonatomic) int tempValue;
@property (nonatomic) bool animating;
@property (nonatomic, strong) UIView *glareLayer;
@end

@implementation FlipboardLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.textColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor blackColor];
        self.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:150.0];
        
        //initialize with empty text.
        [self createNewContent:@""];
        [self addSubview:_nextTop];
        [self addSubview:_nextBottom];
        _currentTop = _nextTop;
        _currentBottom = _nextBottom;
        _nextTop = NULL;
        _nextBottom = NULL;
        _tempValue = -1;
        
        _glareLayer = [[UIView alloc] init];
    }
    return self;
}

-(void)setValue:(NSInteger)newValue
{
    /*
     set: current stays, make new,
            animate, animate done, 
            delete current (if current), 
            current = new, new = null
     */
    //we can only do single-digit numbers to keep it easy.
    if(newValue != _value)
    {
        _value = newValue % 11;
        if(!_animating)
        {
            _animating = true;
            [self animate];
        }
    }
 }

-(void)animate
{
    if(_tempValue == _value)
    {
        //no need to animate.
        _animating = false;
        return;
    }
    _tempValue = (_tempValue+1)%11;
    if(_tempValue == 10)
    {
        [self createNewContent:@""];
    }
    else
    {
        [self createNewContent:[NSString stringWithFormat:@"%d", _tempValue]];
    }
        
    
    [self insertSubview:_nextTop atIndex:0];
    
    [CATransaction begin];
    [_currentTop addSubview:_glareLayer];
    _glareLayer.frame = _nextTop.bounds;
    _glareLayer.backgroundColor = [UIColor whiteColor];
    _glareLayer.alpha = 0.0;
    [self setMaskTo:_glareLayer byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight];
    
    CATransform3D leftTransform = CATransform3DIdentity;
    leftTransform.m34 = -1.0f/1000; //dark magic to set the 3D perspective
    _currentTop.layer.transform = leftTransform;
    _nextTop.layer.zPosition = 10;
    _currentTop.layer.zPosition = 20;
    [CATransaction commit];
   
    //start animating..
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
                     animations:^()
     {
         _glareLayer.alpha = 0.9;
         _currentTop.layer.transform = CATransform3DRotate(leftTransform, M_PI_2, -1, 0, 0);
     }
                     completion:^(BOOL finished)
     {
         [self animateBottomDown];
     }];   
}

-(void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    view.layer.mask = shape;
}

-(void)animateBottomDown
{
    //remove the _currentTop and add newbottom.
    [_currentTop removeFromSuperview];
    [_glareLayer removeFromSuperview];
    [self setMaskTo:_glareLayer byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight];

    [_nextBottom addSubview:_glareLayer];
    
    CATransform3D leftTransform = CATransform3DIdentity;
    leftTransform.m34 = -1.0f/1000; //dark magic to set the 3D perspective
    _nextBottom.layer.transform = CATransform3DRotate(leftTransform, M_PI_2, 1, 0, 0);
    [self addSubview:_nextBottom];
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^()
     {
         _glareLayer.alpha = 0;
         _nextBottom.layer.transform = CATransform3DIdentity;
     }
                     completion:^(BOOL finished)
     {
         //animation complete. clean up.
         [_currentBottom removeFromSuperview];
         [_glareLayer removeFromSuperview];

         _currentTop = _nextTop;
         _currentBottom = _nextBottom;
         
         [self animate];
     }];
}

-(void)createNewContent:(NSString *)text
{
    CGFloat h = self.bounds.size.height;
    CGFloat w = self.bounds.size.width;
    
    //create images for the old value and the new value.
    imageData *halves = [self createHalves:text];
    
    _nextTop = [[UIImageView alloc] initWithFrame:CGRectMake(0,h/2, w, h/2)];
    _nextTop.image = halves.topHalf;
    //_nextTop.clipsToBounds = YES;
    _nextTop.contentMode = UIViewContentModeTop;
    _nextTop.layer.anchorPoint = CGPointMake(0.5, 1);
    //_nextTop.alpha = 0.9;
    
    _nextBottom = [[UIImageView alloc] initWithFrame:CGRectMake(0, h/2, w, h/2)];
    _nextBottom.image = halves.bottomHalf;
    _nextBottom.contentMode = UIViewContentModeTop;
    //_nextBottom.clipsToBounds = YES;
    _nextBottom.layer.anchorPoint = CGPointMake(0.5, 0);
    //_nextBottom.alpha = 0.5;
}

-(imageData *)createHalves:(NSString *)text
{
    UIView *container = [[UIView alloc] initWithFrame:self.bounds];
    container.backgroundColor = [UIColor clearColor];
    container.opaque = NO;

    CGFloat h = container.bounds.size.height;
    CGFloat w = container.bounds.size.width;

    UILabel *label = [[UILabel alloc] initWithFrame:container.bounds];
    label.textColor = [self textColor];
    label.font = [self font];
    label.backgroundColor = [self backgroundColor];
    label.layer.cornerRadius = 10.0;
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    [container addSubview:label];
    
    CALayer *lineLayer = [[CALayer alloc] init];
    lineLayer.frame = CGRectMake(0, h/2, w, 1);
    lineLayer.backgroundColor = [self textColor].CGColor;
    lineLayer.opacity = 0.5;
    [label.layer addSublayer:lineLayer];
    
    imageData *result = [[imageData alloc] init];
    
    UIGraphicsBeginImageContextWithOptions(container.bounds.size, container.opaque, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [container.layer renderInContext:context];
    UIImage *outputImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h/2), container.opaque, 0.0);
    context = UIGraphicsGetCurrentContext();
    [outputImg drawInRect:CGRectMake(0, 0, w, h)];
    result.topHalf = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h/2), container.opaque, 0.0);
    context = UIGraphicsGetCurrentContext();
    [outputImg drawInRect:CGRectMake(0, -h/2, w, h)];
    result.bottomHalf = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
